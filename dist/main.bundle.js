webpackJsonp(["main"],{

/***/ "../../../../../src/$$_gendir lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_gendir lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- APP COMPONENT -->\n<div class=\"container\">\n    <div id=\"challenge-header\">\n        <h1>\n            {{title}}\n        </h1>\n        <p>by {{author}}</p>\n    </div>\n\n    <!-- CHALLENGE COMPONENT -->\n    <div id=\"challenge-body\" class=\"container\">\n        <app-challenge></app-challenge>\n    </div>\n    <!-- /CHALLENGE COMPONENT -->\n\n</div>\n<!-- /APP COMPONENT -->\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'Frontline Code Challenge';
        this.author = 'Ben Fortner';
    }
    return AppComponent;
}());
AppComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.css")]
    })
], AppComponent);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__challenge_challenge_component__ = __webpack_require__("../../../../../src/app/challenge/challenge.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_3__challenge_challenge_component__["a" /* ChallengeComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */]
        ],
        providers: [],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/challenge/challenge.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/challenge/challenge.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <p style=\"text-align:center\">\n        <span style=\"font-weight: bold;\">String to parse:</span> {{stringToParse}}\n    </p>\n</div>\n<div class=\"row\">\n    <div class=\"col-md-3 col-md-offset-3 col-sm-6\">\n        <h2>Problem</h2>\n        <p style=\"padding-left:15px;\">\n            <span style=\"display: block; text-align: left\" *ngFor=\"let key of arrayLiteral\">{{key}}</span>\n        </p>\n    </div>\n    <div class=\"col-md-5 col-md-offset-1 col-sm-6\">\n        <h2>Bonus</h2>\n        <p style=\"padding-left:15px;\">\n            <span style=\"display: block;\" *ngFor=\"let key of arrayAlpha\">{{key}}</span>\n        </p>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/challenge/challenge.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChallengeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__challengeParser__ = __webpack_require__("../../../../../src/app/challengeParser.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__challengePrinter__ = __webpack_require__("../../../../../src/app/challengePrinter.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ChallengeComponent = (function () {
    function ChallengeComponent() {
        /**
         * String to parse for output
         * @type {string}
         */
        this.stringToParse = '(id,created,employee(id,firstname,employeeType(id), lastname),location)';
        /**
         * Values used for literal solution of problem
         * @type {Array}
         */
        this.arrayLiteral = [];
        /**
         * Array of values for alphabetical sort (bonus problem)
         * @type {Array}
         */
        this.arrayAlpha = [];
        // instantiate challengeParser class
        var challengeParser = new __WEBPACK_IMPORTED_MODULE_1__challengeParser__["a" /* ChallengeParser */](this.stringToParse);
        // instantiate ChallengeFormatter class
        var challengeFormatter = new __WEBPACK_IMPORTED_MODULE_2__challengePrinter__["a" /* ChallengeFormatter */]();
        // object returned from parse function
        var outputObject = challengeParser.parseString();
        // set literal output
        this.arrayLiteral = challengeFormatter.convertAndSort(outputObject, false);
        // set alpha sorted output
        this.arrayAlpha = challengeFormatter.convertAndSort(outputObject, true);
    }
    ChallengeComponent.prototype.ngOnInit = function () {
    };
    return ChallengeComponent;
}());
ChallengeComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'app-challenge',
        template: __webpack_require__("../../../../../src/app/challenge/challenge.component.html"),
        styles: [__webpack_require__("../../../../../src/app/challenge/challenge.component.css")]
    })
    /**
     * This component is for the execution of the code challenge presented by Frontline.
     *
     */
    ,
    __metadata("design:paramtypes", [])
], ChallengeComponent);

//# sourceMappingURL=challenge.component.js.map

/***/ }),

/***/ "../../../../../src/app/challengeParser.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChallengeParser; });
/**
 * Class used for parsing the string presented in the challenge
 * Loops through the string by character and takes action based if the character is
 * a comma, closed parenthesis, or open parenthesis.
 *
 */
var ChallengeParser = (function () {
    function ChallengeParser(stringToParse) {
        /**
         * Array of objects created during parsing
         * @type {Array}
         */
        this.arrayObjects = [];
        /**
         * Array of functions to be called by separator
         * @type {Array}
         */
        this.arrayFunctionsBySeparator = [];
        /**
         * Creates new key from currentItem with value of null in arrayObjects
         */
        this.insertKey = function () {
            if (this.currentItem.length > 0) {
                this.arrayObjects[this.arrayObjects.length - 1][this.currentItem] = null;
            }
            this.currentItem = '';
        };
        /**
         * Creates new object in arrayObjects
         */
        this.insertObject = function () {
            var newObject = {}; // empty object
            if (this.arrayObjects.length > 0 && this.currentItem.length > 0) {
                this.arrayObjects[this.arrayObjects.length - 1][this.currentItem] = newObject;
            }
            // add new object
            this.arrayObjects.push(newObject);
            this.currentItem = '';
        };
        /**
         * Closes the current object
         */
        this.closeObject = function () {
            this.insertKey(); // add key
            this.objectFinal = this.arrayObjects.pop();
            this.currentItem = '';
        };
        this.stringToParse = stringToParse;
        // set functions by separators
        this.arrayFunctionsBySeparator[','] = this.insertKey;
        this.arrayFunctionsBySeparator['('] = this.insertObject;
        this.arrayFunctionsBySeparator[')'] = this.closeObject;
    }
    /**
     * Parses string provided during instantiation
     * @returns {any}
     */
    ChallengeParser.prototype.parseString = function () {
        // loop through each character in string
        for (var i = 0; i < this.stringToParse.length; i++) {
            this.parseChar(this.stringToParse.charAt(i));
        }
        return this.objectFinal;
    };
    /**
     * Calls function to createKey, insertObject, or close object based on the input character
     * "," = insertKey
     * "(" = insertObject
     * ")" = closeObject
     * @param {string} char
     */
    ChallengeParser.prototype.parseChar = function (char) {
        var charFunction = this.arrayFunctionsBySeparator[char]; // function to call
        // if function exists call
        // else if not a space, add to current item
        if (charFunction) {
            charFunction.call(this);
        }
        else if (' ' !== char) {
            this.currentItem += char;
        }
    };
    return ChallengeParser;
}());

//# sourceMappingURL=challengeParser.js.map

/***/ }),

/***/ "../../../../../src/app/challengePrinter.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChallengeFormatter; });
/**
 * Formats a given object by depth and sorts by index or alphabetically.
 */
var ChallengeFormatter = (function () {
    function ChallengeFormatter() {
        /**
         * Sorts a list alphabetically
         * @param a
         * @param b
         * @returns {number}
         */
        this.sortAlphabetically = function (a, b) {
            return a.localeCompare(b);
        };
        /**
         * Creates a string of the keys in the provided object adding leading
         * characters per the depth of the key within the object
         *
         * @param object
         * @param depth
         * @returns {string}
         */
        this.createOutputString = function (object, depth) {
            var stringOut = ''; // string returned
            var sortedKeys = this.getSortedKeys(object); // list of keys sorted by depth
            var depthString = this.getDepthChar(depth); // char to add based on depth
            var currentKey; // holds current key
            // loop through the object keys and create the new string
            for (var i = 0; i < sortedKeys.length; i++) {
                currentKey = sortedKeys[i];
                // add the depth string to the output
                if (depth > 0) {
                    stringOut += depthString + ' ';
                }
                // add the current key and EOL indicator
                stringOut += currentKey + '%';
                // if we need to go deeper
                if (null != object[currentKey]) {
                    stringOut += this.createOutputString(object[currentKey], depth + 1);
                }
            }
            return stringOut;
        };
        /**
         * Gets array of keys sorted by index or alphabetically
         * @param object
         * @returns {string[]}
         */
        this.getSortedKeys = function (object) {
            var keys = Object.keys(object); // index based array of keys
            // sort alphabetically if flag is set
            if (this.sortFunction) {
                keys.sort(this.sortFunction);
            }
            return keys;
        };
        /**
         * Gets leading character based on depth
         * @param depth
         * @returns {string}
         */
        this.getDepthChar = function (depth) {
            var depthChar = '';
            // add leading character for every level of depth
            for (var i = 0; i < depth; i++) {
                depthChar += '-';
            }
            return depthChar;
        };
    }
    /**
     * Converts the given object to an array and sorts alphabetically per alphaSort flag
     *
     * @param object
     * @param alphaSort
     * @returns {string[]}
     */
    ChallengeFormatter.prototype.convertAndSort = function (object, alphaSort) {
        // set alphabetical sort function if requested
        if (alphaSort === true) {
            this.sortFunction = this.sortAlphabetically;
        }
        // get formated string
        var strOut = this.createOutputString(object, 0);
        // convert string to array and return
        return strOut.split('%');
    };
    return ChallengeFormatter;
}());

//# sourceMappingURL=challengePrinter.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_19" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map