/**
 * Class used for parsing the string presented in the challenge
 * Loops through the string by character and takes action based if the character is
 * a comma, closed parenthesis, or open parenthesis.
 *
 */
export class ChallengeParser {

    /**
     * The name of the item currently being evaluated
     * @type {String}
     */
    private currentItem: String;

    /**
     * Array of objects created during parsing
     * @type {Array}
     */
    private arrayObjects: Array<any> = [];

    /**
     * Holds the last object created during parsing
     */
    private objectFinal: null;

    /**
     * Array of functions to be called by separator
     * @type {Array}
     */
    private arrayFunctionsBySeparator: Array<any> = [];

    /**
     * String to be parsed
     * @type {String}
     */
    private stringToParse: String;

    constructor(stringToParse) {
        this.stringToParse = stringToParse;

        // set functions by separators
        this.arrayFunctionsBySeparator[','] = this.insertKey;
        this.arrayFunctionsBySeparator['('] = this.insertObject;
        this.arrayFunctionsBySeparator[')'] = this.closeObject;
    }

    /**
     * Parses string provided during instantiation
     * @returns {any}
     */
    parseString() {
        // loop through each character in string
        for (let i = 0; i < this.stringToParse.length; i++) {
            this.parseChar(this.stringToParse.charAt(i));
        }

        return this.objectFinal;
    }

    /**
     * Creates new key from currentItem with value of null in arrayObjects
     */
    insertKey = function () {
        if (this.currentItem.length > 0) {
            this.arrayObjects[this.arrayObjects.length - 1][this.currentItem] = null;
        }
        this.currentItem = '';
    };

    /**
     * Creates new object in arrayObjects
     */
    insertObject = function () {
        const newObject = {}; // empty object

        if (this.arrayObjects.length > 0 && this.currentItem.length > 0) {
            this.arrayObjects[this.arrayObjects.length - 1][this.currentItem] = newObject;
        }

        // add new object
        this.arrayObjects.push(newObject);
        this.currentItem = '';
    };

    /**
     * Closes the current object
     */
    closeObject = function () {
        this.insertKey(); // add key
        this.objectFinal = this.arrayObjects.pop();
        this.currentItem = '';
    };

    /**
     * Calls function to createKey, insertObject, or close object based on the input character
     * "," = insertKey
     * "(" = insertObject
     * ")" = closeObject
     * @param {string} char
     */
    parseChar(char: string) {
        const charFunction = this.arrayFunctionsBySeparator[char]; // function to call

        // if function exists call
        // else if not a space, add to current item
        if (charFunction) {
            charFunction.call(this);
        } else if (' ' !== char) {
            this.currentItem += char;
        }
    }

}
