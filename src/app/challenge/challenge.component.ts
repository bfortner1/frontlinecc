import {Component, OnInit} from '@angular/core';
import {ChallengeParser} from '../challengeParser';
import {ChallengeFormatter} from '../challengeFormatter';

@Component({
    selector: 'app-challenge',
    templateUrl: './challenge.component.html',
    styleUrls: ['./challenge.component.css']
})

/**
 * This component is for the execution of the code challenge presented by Frontline.
 *
 */
export class ChallengeComponent implements OnInit {

    /**
     * String to parse for output
     * @type {string}
     */
    stringToParse = '(id,created,employee(id,firstname,employeeType(id), lastname),location)';

    /**
     * Values used for literal solution of problem
     * @type {Array}
     */
    arrayLiteral = [];

    /**
     * Array of values for alphabetical sort (bonus problem)
     * @type {Array}
     */
    arrayAlpha = [];

    constructor() {
        // instantiate challengeParser class
        let challengeParser = new ChallengeParser(this.stringToParse);

        // instantiate ChallengeFormatter class
        let challengeFormatter = new ChallengeFormatter();

        // object returned from parse function
        let outputObject = challengeParser.parseString();

        // set literal output
        this.arrayLiteral = challengeFormatter.convertAndSort(outputObject, false);

        // set alpha sorted output
        this.arrayAlpha = challengeFormatter.convertAndSort(outputObject, true);
    }

    ngOnInit() {

    }

}
