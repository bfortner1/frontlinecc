/**
 * Formats a given object by depth and sorts by index or alphabetically.
 */
export class ChallengeFormatter {

    /**
     * function to sort by
     */
    sortFunction;

    constructor() {}

    /**
     * Sorts a list alphabetically
     * @param a
     * @param b
     * @returns {number}
     */
    sortAlphabetically = function (a, b) {
        return a.localeCompare(b);
    };

    /**
     * Creates a string of the keys in the provided object adding leading
     * characters per the depth of the key within the object
     *
     * @param object
     * @param depth
     * @returns {string}
     */
    createOutputString = function (object, depth) {
        let stringOut = ''; // string returned
        let sortedKeys = this.getSortedKeys(object); // list of keys sorted by depth
        let depthString = this.getDepthChar(depth); // char to add based on depth
        let currentKey; // holds current key

        // loop through the object keys and create the new string
        for (let i = 0; i < sortedKeys.length; i++) {
            currentKey = sortedKeys[i];

            // add the depth string to the output
            if (depth > 0) {
                stringOut += depthString + ' ';
            }

            // add the current key and EOL indicator
            stringOut += currentKey + '%';

            // if we need to go deeper
            if (null != object[currentKey]) {
                stringOut += this.createOutputString(object[currentKey], depth + 1);
            }
        }

        return stringOut;
    };

    /**
     * Gets array of keys sorted by index or alphabetically
     * @param object
     * @returns {string[]}
     */
    getSortedKeys = function (object) {
        let keys = Object.keys(object); // index based array of keys

        // sort alphabetically if flag is set
        if (this.sortFunction) {
            keys.sort(this.sortFunction);
        }

        return keys;
    };

    /**
     * Gets leading character based on depth
     * @param depth
     * @returns {string}
     */
    getDepthChar = function (depth) {
        let depthChar = '';

        // add leading character for every level of depth
        for (let i = 0; i < depth; i++) {
            depthChar += '-';
        }

        return depthChar;
    };

    /**
     * Converts the given object to an array and sorts alphabetically per alphaSort flag
     *
     * @param object
     * @param alphaSort
     * @returns {string[]}
     */
    convertAndSort(object, alphaSort) {

        // set alphabetical sort function if requested
        if (alphaSort === true) {
            this.sortFunction = this.sortAlphabetically;
        }

        // get formated string
        let strOut = this.createOutputString(object, 0);

        // convert string to array and return
        return strOut.split('%');
    }

}
