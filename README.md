# Frontline Code Challenge

Angular based solution of the code challenge presented by Frontline Technologies Group LLC.

#### Solution Logic Files
src/app/challengeParser.ts

src/app/challengeFormatter.ts

src/app/challenge/challenge.component.ts

Frontline Education Code Challenge
Parameters
Should be solved in a language which demonstrates your skill for the position you have applied.
Deliver a working runnable solution and include a copy of the source code.
Write code typical of something we would be proud to have in Frontline software in production.
You will need to independently overcome any challenges you face in delivery. 
If applicable, please list your assumptions.
Problem to Solve
Convert the string: 
```
"(id,created,employee(id,firstname,employeeType(id), lastname),location)" 
```
to the following output:
```
id
created
employee
- id
- firstname
- employeeType
-- id
- lastname
location
```
Bonus (output in alphabetical order):
```
created
employee
- employeeType
-- id
- firstname
- id
- lastname
id
location
```

## Author

* **Ben Fortner** - [LinkedIn](https://www.linkedin.com/in/agilesoftware)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
